package com.socialgroup.discovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class SocialGroupDiscoveryApplication {

	public static void main(final String[] args) {
		SpringApplication.run(SocialGroupDiscoveryApplication.class, args);
	}

}
